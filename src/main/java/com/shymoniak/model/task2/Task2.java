package com.shymoniak.model.task2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Using the documentation for java.util.regex.Pattern as a resource,
 * write and test a regular expression that checks a sentence to see that
 * it begins with a capital letter and ends with a period('.').
 */
public class Task2 {

    public boolean startsWithCapitalEndsPeriod(String sentence){
        Pattern pattern = Pattern.compile("^[A-Z].*.");
        Matcher m = pattern.matcher(sentence);
        return m.lookingAt();
    }
}
