package com.shymoniak.model.task6.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Word {
    private String word;

    public boolean isWord(String check){
        Pattern pattern = Pattern.compile("[a-zA-Z]");
        Matcher m = pattern.matcher(check);
        return m.matches();
    }
}
