package com.shymoniak.model.task6.entity;

import com.shymoniak.model.task2.Task2;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Sentence {
    private String sentence;

    public boolean isSentence(String check){
        return new Task2().startsWithCapitalEndsPeriod(check);
    }
}
