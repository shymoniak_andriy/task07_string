package com.shymoniak.model.task6;

import com.shymoniak.model.task6.entity.PunctuationMark;
import com.shymoniak.model.task6.entity.Sentence;
import com.shymoniak.model.task6.entity.Word;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * - Створити програму опрацювання тексту підручника з програмування з
 * використанням класів: слово, речення, розділовий знак та ін. У всіх задачах з
 * формуванням тексту замінити табуляції і послідовності пробілів одним пробілом.
 * - Забезпечити введення даних з файлу. Де потрібно, застосуйте jUnit,
 * log4j, exceptions.
 * - Для презентації підготуйте набори текстів для перевірки, які показують
 * всі аспекти роботи програми.
 */
public class Task6 {
    private List<Word> words = new ArrayList<>();
    private List<Sentence> sentences = new ArrayList<>();
    private List<PunctuationMark> punctuationMarks = new ArrayList<>();

    public StringBuilder replaceSpaces(StringBuilder stringBuilder) {
        Pattern pattern = Pattern.compile("[^\t\n\f\\s+]");
        Matcher matcher = pattern.matcher(stringBuilder);
        return new StringBuilder(matcher.replaceAll("\\s"));
    }

    public StringBuilder readFromFile() {
        File file = new File("text.txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        StringBuilder stringBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines(Paths.get(file.getPath()))) {
            stream.forEach(stringBuilder::append);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return stringBuilder;
    }

    public List<Word> getWords(StringBuilder stringBuilder) {
        List<Word> words = new ArrayList<>();

        for (int i = 0; i < stringBuilder.length(); i++) {
            if (!isPunctMarkOrSpace(stringBuilder.charAt(i))) {
                int startIndex = i;
                while (!isPunctMarkOrSpace(stringBuilder.charAt(i))) {
                    if (i + 1 < stringBuilder.length()){
                        i++;
                    } else {
                        break;
                    }
                }
                int endIndex = i;
                words.add(new Word(stringBuilder.substring(startIndex, endIndex)));
            } else {
                if (i + 1 < stringBuilder.length()){
                    i++;
                } else {
                    break;
                }
            }
        }
        return words;
    }

    private boolean isPunctMarkOrSpace(char c) {
        return c == ' ' || new PunctuationMark().isPunctuationMark(String.valueOf(c));
    }

}