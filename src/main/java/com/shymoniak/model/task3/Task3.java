package com.shymoniak.model.task3;

import org.apache.logging.log4j.LogManager;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Split some string on the words "the" or "you".
 */
public class Task3 {
    private static final String WORD1 = "the";
    private static final String WORD2 = "you";

    public List<String> splitStrings(String sentence) {
        String regex1 = new String("\\s*" + WORD1 + "\\s*");
        String regex2 = new String("\\s*" + WORD2 + "\\s*");

        List<String> res = Arrays.asList(sentence.split(regex1));

        res = res.stream()
                .flatMap(Pattern.compile(regex2)::splitAsStream)
                .filter(s -> !s.equals(""))
                .collect(Collectors.toList());

        return res;
    }

    public void printList(List<Integer> list) {
        System.out.println(list.size());
        list.stream().forEach(s -> LogManager.getLogger(Task3.class).trace(s + ", "));
    }
}
