package com.shymoniak.model;

import com.shymoniak.model.task1.Task1;
import com.shymoniak.model.task2.Task2;
import com.shymoniak.model.task3.Task3;
import com.shymoniak.model.task4.Task4;
import com.shymoniak.model.task5.Task5;
import com.shymoniak.model.task6.Task6;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 1. Create class StringUtils with an undefined number of parameters of
 *    any class that concatenates all parameters and returns Strings.
 *    Internationalize menu in your program for a few languages.
 *    https://native2ascii.net/
 * 2. Using the documentation for java.util.regex.Pattern as a resource,
 *    write and test a regular expression that checks a sentence to see that
 *    it begins with a capital letter and ends with a period.
 * 3. Split some string on the words "the" or "you".
 * 4. Replace all the vowels in some text with underscores.
 * 5. Solve the RexEx Crossword: http://uzer.com.ua/cross/
 * 6. Make BigTask with Regular Expression.
 */
public class MainModel {
    private static final Logger LOGGER = LogManager.getLogger(MainModel.class);
    Task1 task1 = new Task1();
    Task2 task2 = new Task2();
    Task3 task3 = new Task3();
    Task4 task4 = new Task4();
    Task5 task5 = new Task5();
    Task6 task6 = new Task6();

    public void printTask1(){

    }

    public void printTask2(String sentence) {
        System.out.println(task2.startsWithCapitalEndsPeriod(sentence));
    }

    public void printTask3(String sentence) {
        System.out.println(task3.splitStrings(sentence));
    }

    public void printTask4(String sentence){
        System.out.println(task4.replaceVowels(sentence));
    }

    public void printTask6(){
        task6.getWords(new StringBuilder("Hello my name - is ? ffs?"));
    }
}
