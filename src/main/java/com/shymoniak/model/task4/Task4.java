package com.shymoniak.model.task4;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Replace all the vowels in some text with underscores.
 */
public class Task4 {

    public String replaceVowels(String sentence) {
        return sentence.replaceAll("[aeio]", "_");
    }
}
