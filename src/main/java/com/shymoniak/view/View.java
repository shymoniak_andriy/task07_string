package com.shymoniak.view;

import com.shymoniak.model.MainModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class View {
    private static final Logger LOGGER = LogManager.getLogger(View.class);
    MainModel mainModel = new MainModel();
    Scanner scan = new Scanner(System.in);

    public void startApp() {
        LOGGER.info("Enter a sentence");
//        mainModel.printTask3(scan.nextLine());
        mainModel.printTask6();
    }

}
